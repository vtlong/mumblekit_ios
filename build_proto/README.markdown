// Build protoc for MumbleKit

1 - Build protobuf 2.5.1
	1.1: ./autogen.sh
	1.2: ./configure --disable-shared
	1.3: make
	
2 - Build proto-objc-out
	2.1: ./autogen.sh
	2.2: ./configure CXXFLAGS=-I/Volumes/Data/Projects/talking-with-strangers/StrangerChat/StrangerChat/ExtLibs/MumbleKit/build_proto/protobuf-2.5.0/src  LDFLAGS=-L/Users/longvt/Downloads/protobuf-2.5.0/src/.libs
	2.3: make