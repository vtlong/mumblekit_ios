//
//  MKCommand.m
//  MumbleKit (iOS)
//
//  Created by LongVT on 8/24/18.
//

#import "MKCommand.h"

@interface MKCommand () {
    
}

@end;

@implementation MKCommand

- (instancetype)init
{
    self = [super init];
    _cmdType = 0;
    _senderId = 0;
    _cmdData = -1;
    _jsonData = nil;
    
    return self;
}

@end
