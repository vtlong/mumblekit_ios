//
//  MKCommand.h
//  MumbleKit (iOS)
//
//  Created by LongVT on 8/24/18.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, CommandType) {
    CMD_Sitdown = 1,
    CMD_Talking = 2,
};

@interface MKCommand : NSObject

@property (nonatomic) NSInteger cmdType;
@property (nonatomic) NSUInteger senderId;
@property (nonatomic) NSInteger cmdData;
@property (nonatomic, copy) NSString* jsonData;

@end
